#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','3500M');
set_time_limit ( 60 * 60 ) ; // Seconds
require_once ( '../public_html/php/common.php' ) ;
require_once ( '../public_html/php/wikidata.php' ) ;

$batch_size = 2000 ;
$max_candidates = 8 ;
$min_close_items = 5 ;
$query = "SELECT DISTINCT ?items WHERE {
  ?items wdt:P31/wdt:P279+ wd:Q33506.
  FILTER(NOT EXISTS { ?items wdt:P131 [] })
}" ;
$querylocalidades = "SELECT ?items WHERE {
  ?items wdt:P31/wdt:P279* wd:Q486972.
  ?items wdt:P17 ?pais_latam.
  ?pais_latam wdt:P361 wd:Q12585.
  FILTER(EXISTS { ?items wdt:P625 [] })
  FILTER(NOT EXISTS { ?items wdt:P131 [] })
}" ;

$testing = false ;
$items = array() ;

if ( isset($argv[1]) ) {
	$testing = true ;
	$items = array ( preg_replace ( '/\D/' , '' , $argv[1] ) ) ;
} else {
	$j = getSPARQL($query);
	$itemstmp = $j->results->bindings ;
	unset ( $j ) ;
	foreach ( $itemstmp AS $i ) {
		$t = $i->items->value;
		$t = preg_replace ( '/\D/' , '' , "$t" ) ;
		$items[$t] = $t ;
	}
	unset ( $itemstmp );
	print "Items: ".count ( $items )."\n";
	shuffle ( $items ) ;
	$items = array_slice ( $items , 0 , $batch_size ) ;
}

$db = openToolDB ( 'wdgames' ) ;

// Remove the ones we already have
if ( !$testing ) {
	$sql = "SELECT * FROM coord_no_admin_unit_museums WHERE q IN (" . implode(',',$items) . ")" ;
	if(!$result = $db->query($sql)) {
		die('There was an error running the query [' . $db->error . ']');
		exit(1);
	}
	while($o = $result->fetch_object()){
		if ( ($k=array_search($o->q, $items)) !== false ) {
			print "Quitando ".$items[$k]."\n";
//			$sql = "UPDATE coord_no_admin_unit_museums set done = 0 where q = $items[$k]" ;
	
//			if ( !$db->ping() ) $db = openToolDB ( 'wdgames' ) ;
//			if(!$res = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
			unset($items[$k]) ;
		}
	}
}

$wdi = new WikidataItemList ;
$wdi->loadItems ( $items ) ;

$p131 = '131' ;
foreach ( $items AS $q ) {
	print "Q: ".$q."\n";
	if ( !$wdi->hasItem ( $q ) ) continue ;
	$i = $wdi->getItem ( $q ) ;
	if ( !$i->hasClaims('P625') ) { print "Has no coords\n" ; continue ; }
	if ( $i->hasClaims('P131') ) { print "Has admin unit\n" ; continue ; }
	$c = $i->getClaims('P625') ;
	$c = $c[0] ;
	if ( !isset($c->mainsnak) ) continue ;
	if ( !isset($c->mainsnak->datavalue) ) continue ;
	if ( !isset($c->mainsnak->datavalue->value) ) continue ;
	$c = $c->mainsnak->datavalue->value ;
	
	$candidates = array() ;
	for ( $radius = 1 ; $radius < 55 && count($candidates) < $max_candidates ; $radius += 5 ) {
		$tmp = getSPARQL('SELECT ?items WHERE {
			wd:Q'.$q.' wdt:P625 ?loc .
			SERVICE wikibase:around {
			      ?place wdt:P625 ?location .
			      bd:serviceParam wikibase:center ?loc .
			      bd:serviceParam wikibase:radius "'.$radius.'" .
			}
			?place wdt:P131 ?items .
			BIND(geof:distance(?loc, ?location) as ?dist)
		}');
		$j = $tmp->results->bindings;
		//var_dump($j);
		//if ( preg_match ( '/\{\[\]\}/' , $t ) ) continue ; // No admin regions
		//$j = json_decode ( $t ) ;
		if ( count($j) < $min_close_items ) continue ;
		foreach ( $j AS $v ) {
			$t = $v->items->value;
			$t = preg_replace ( '/\D/' , '' , "$t" ) ;
			if ( $t == $q ) continue ;
			//print "t: ".$t."\n";
			if ( count($candidates) < $max_candidates )
				$candidates[$t] = $t ;
		}
		if ( $testing ) print "Close items within $radius km: " . count($j) . "\n" ;
		if ( count($candidates) == 0 ) continue ;
		break ;
	}
	
	if ( $testing )  print_r ( $candidates ) ;
	
	if ( count ( $candidates ) == 0 ) {
		print "no candidates\n";
		continue ;
	}
	if ( count ( $candidates ) > $max_candidates ){
		 print "max_cand\n";
		 continue ;
	}
	
	$sql = "INSERT IGNORE INTO coord_no_admin_unit_museums (q,lat,lon,candidates,random) VALUES ($q,{$c->latitude},{$c->longitude},'" . implode(',',$candidates) . "',rand())" ;
	
	if ( $testing ) {
		print "$sql\n" ;
		exit ( 0 ) ;
	}
	
	if ( !$db->ping() ) $db = openToolDB ( 'wdgames' ) ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
}

//$sql = "UPDATE coord_no_admin_unit_museums SET random=rand() WHERE random IS NULL" ;
//if(!$result = $dbu->query($sql)) die('There was an error running the query [' . $db->error . '] '.$sql);

?>

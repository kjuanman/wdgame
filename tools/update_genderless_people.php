#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
require_once ( '../public_html/php/common.php' ) ;

$min_label_length = 4 ;
$max_num_dupes = 4 ;

//$db = openDB ( 'wikidata' , 'wikidata' ) ;
$dbu = openToolDB ( 'wdgames' ) ;


//$sql = "select DISTINCT epp_entity_id from wb_entity_per_page,pagelinks WHERE epp_entity_id IN (" . implode(',',$all_items) . ") AND epp_page_id=pl_from AND pl_namespace=0 AND pl_title IN ($wp_types)" ;
$existing = array() ;
$sql = "SELECT item FROM gender_latam" ;
if(!$result = $dbu->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$existing[$o->item] = 1 ;
}

print count($existing) . " items in DB\n" ;


//$url = "$wdq_internal_url?q=CLAIM[31:5]%20AND%20NOCLAIM[21]%20AND%20CLAIM[27:414]" ;
//$j = json_decode ( file_get_contents ( $url ) ) ;
//$j = getSPARQL( 'SELECT%20%3Fser_humano%20%3Fser_humanoLabel%20WHERE%20%7B%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20"%5BAUTO_LANGUAGE%5D%2Cen".%20%7D%0A%20%20%3Fser_humano%20wdt%3AP31%20wd%3AQ5.%0A%20%20%3Fser_humano%20wdt%3AP27%20%3Fpais_latam.%0A%20%20%3Fpais_latam%20wdt%3AP31%20wd%3AQ6256.%0A%20%20%3Fpais_latam%20wdt%3AP361%20wd%3AQ12585%20.%0A%20%20MINUS%20%7B%20%3Fser_humano%20wdt%3AP21%20%5B%5D%20%7D%20%0A%7D' );
  //?items wdt:P31 wd:Q5.
  //?items wdt:P19 ?lugar_latam.
  //?lugar_latam wdt:P31/wdt:P279* wd:Q486972.
  //?lugar_latam wdt:P17 ?pais_latam.
  //?pais_latam wdt:P361 wd:Q12585.  
$tmp = getSPARQL('SELECT DISTINCT ?items WHERE {
  ?items wdt:P31 wd:Q5.
  ?items wdt:P27 ?pais_latam.
  ?pais_latam wdt:P31 wd:Q6256.
  ?pais_latam wdt:P361 wd:Q12585 .
  MINUS { ?items wdt:P21 [] } 
} ');
//print $j->results->bindings[0];
$j = $tmp->results->bindings;
//var_dump($j);
if ( !isset($j) or count($j) == 0 ) exit ( 0 ) ; // Paranoia

print count($j) . " items from WDQ\n" ;
//print $j[0] . "\n" ;

$create = array() ;
foreach ( $j AS $i ) {
	$t = $i->items->value;
	$t = preg_replace ( '/\D/' , '' , "$t" ) ;
	if ( isset ( $existing[$t] ) ) $existing[$t] = "OK" ;
	else $create[] = $t ;
}

$remove = array() ;
foreach ( $existing AS $i => $s ) {
	if ( $s != 'OK' ) $remove[] = $i ;
}

print "Creating " . count($create) . ", remove " . count($remove) . "\n" ;

//$dbu->beginTransaction();
if ( count($create) > 0 ) {
	$sql = "INSERT IGNORE INTO gender_latam (item) VALUES (" . implode("),(",$create) . ")" ;
	if(!$result = $dbu->query($sql)) die('There was an error running the query [' . $db->error . ']');
}
//if ( count($remove) > 0 ) {
//	$sql = "DELETE FROM gender_latam WHERE status IS NULL AND item IN (" . implode(",",$remove) . ")" ;
//	if(!$result = $dbu->query($sql)) die('There was an error running the query [' . $db->error . ']');
//}
//$dbu->commit();

//$sql = "UPDATE gender_latam SET random=rand() WHERE random IS NULL" ;
$sql = "UPDATE gender_latam SET random=rand()" ;
if(!$result = $dbu->query($sql)) die('There was an error running the query [' . $db->error . '] '.$sql);

?>

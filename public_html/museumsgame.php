<?PHP

require_once ( './game1.inc' ) ;
require_once ( './php/wikidata.php' ) ;

header('Content-type: application/json');

$callback = get_request ( 'callback' , '' ) ;
$action = get_request ( 'action' , '' ) ;

$out = array () ;
$wil = new WikidataItemList ;
$strings = array (
	"notapplicable" => array ( "es" => "No aplica", "pt" => "Não aplicável", "en" => "Non Applicable" ),
	"notlisted" => array ( "es" => "No está listado, o no aplica", "pt" => "Não está listado, ou não aplica", "en" => "Not listed, or does not apply" ),
	"skip" => array ( "es" => "Saltear", "pt" => "Pular", "en" => "Skip" ),
	"genderless" => array ( "es" => "Personas sin género", "pt" => "Pessoas sem gênero", "en" => "Genderless people" ),
	"adminunit" => array ( "es" => "Lugares sin entidad administrativa", "pt" => "Lugares sem entidade administrativa", "en" => "Places without administrative unit" )
	);

if ( $action == 'desc' ) {
	$lang = get_request('lang','en') ; // The language to use, with 'en' as fallback

	$out = array (
		"label" => array ( "en" => "International Museum Day Competition",
				   "es" => "Competición Día Internacional de los Museos"
				    ) ,
		"description" => array ( "en" => "Set administrative units for Museums.<br>
Some museums have coordinates but no administrative unit. Units will be suggested to you based on nearby items.<br>
The competition starts on 4 May 2022 and ends on 18 May 2022. [https://www.wikidata.org/wiki/Wikidata:Events/International_Museum_Day_2022 More info].",
					 "es" => "El concurso tiene como objetivo mejorar información en Wikidata sobre museos. 
<ul>
<li>Lugares sin entidad administrativa: seleccionar la entidad territorial administrativa a la que pertenece. Debe seleccionarse la dependencia inmediatamente superior. Ej: en Argentina es preferible seleccionar, si existe, un municipio/departamento/partido antes que la provincia; en Bolivia es preferible seleccionar la provincia ya que son más pequeñas que los departamentos.</li>
</ul>
La competencia comienza el 4 de mayo y termina el 18 de mayo de 2022.
Solo se tomará en cuenta las ediciones realizadas en ese período.
[https://www.wikidata.org/wiki/Wikidata:Events/International_Museum_Day_2022/es Más info]"
 ) ,
		"icon" => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/FindingGLAMs_logo.svg/128px-FindingGLAMs_logo.svg.png'
//                "options" => array (
//		        array ( 'name' => 'Juego' , 'key' => 'type' , 'values' => array ( 'genderless' => $strings["genderless"][$lang], 'adminunit' => $strings["adminunit"][$lang] ) )
//		)

	) ;

} else if ( $action == 'tiles' ) {
	$db = openToolDB ( 'wdgames' ) ;
	$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;

	// GET parameters
	$num = get_request('num',1)*1 ; // Number of games to return
	$lang = get_request('lang','en') ; // The language to use, with 'en' as fallback; ignored in this game
	if( $lang != 'es' ){
		$lang = 'en';
	}
//	$type = get_request('type','any') ; // The type of game
	$type = 'adminunit';
	$hadthat = array() ;

	$out['tiles'] = array() ;
	while ( count($out['tiles']) < $num ) {
	   if( $type == 'genderless' ){
	    }
	    else {
		$r = rand() / getrandmax() ;
		$tmp = array() ;
		$sqls = array() ;
		$use_labels = array() ;
		$sql = "SELECT * FROM coord_no_admin_unit_museums WHERE done=0 AND random >= $r " ;
		if ( count ( $hadthat ) > 0 ) $sql .= " AND id NOT IN (" . implode(',',$hadthat) . ") " ;
		$sql .= " ORDER BY random LIMIT " . ($num*2) ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($o = $result->fetch_object()){
			// Sanity checks
			if ( isDeleted ( $dbwd , $o->q ) or isRedirect ( $dbwd , $o->q ) or hasLink ( $dbwd , $o->q , 'P131' ) ) {
				$sqls[] = "UPDATE coord_no_admin_unit_museums SET done=1 WHERE id=" . $o->id ;
				continue ;
			}
			$o->candidates = explode ( ',' , $o->candidates ) ;
			$tmp[] = $o ;
			foreach ( $o->candidates AS $c ) $use_labels["Q$c"] = "Q$c" ;
		}
		foreach ( $sqls AS $sql ) $db->query($sql) ; // Clean-up
		if ( count($tmp) == 0 ) continue ;
		
		$wil->loadItems ( $use_labels ) ;

		foreach ( $tmp AS $o ) {
			$hadthat[] = $o->id ;
			$g = array(
				'id' => $o->id ,
				'sections' => array () ,
				'controls' => array ()
			) ;
		
			$q = 'Q'.$o->q ;
			
			$entries = array() ;
			foreach ( $o->candidates AS $c ) {
				$i = $wil->getItem($c) ;
				$label = "Q$c" ;
				if ( $label == $q ) continue ; // Not in self...
				if ( isset($i) ) $label = $i->getLabel($lang) ;
				$a = array ( 'type' => 'green' , 'decision' => 'yes' , 'label' => $label , 'api_action' => 
					array ('action'=>'wbcreateclaim','entity'=>"$q",'snaktype'=>'value','property'=>'P131',
						'value'=>'{"entity-type":"item","numeric-id":'.$c.'}'
					)
				) ;
				$entries[] = $a ;
			}
		
			$g['sections'][] = array ( 'type' => 'map' , 'lat' => $o->lat , 'lon' => $o->lon , 'zoom' => $o->zoom ) ;
			$g['sections'][] = array ( 'type' => 'item' , 'q' => $q ) ;
			$g['controls'][] = array ( 'type' => 'buttons' , 'entries' => $entries ) ;
			$g['controls'][] = array (
				'type' => 'buttons' ,
				'entries' => array (
					array ( 'type' => 'white' , 'decision' => 'skip' , 'label' => $strings['skip'][$lang] ) ,
					array ( 'type' => 'blue' , 'decision' => 'no' , 'label' => $strings['notlisted'][$lang] )
				)
			) ;
		
			$out['tiles'][] = $g ;
			
			if ( count($out['tiles']) == $num ) break ;
		}
	    }
	}

} else if ( $action == 'log_action' ) {

    $type = get_request('type','any') ; // The type of game

    if( $type == 'genderless' ){
    }
    else {
	$db = openToolDB ( 'wdgames' ) ;
	$user = $db->real_escape_string ( get_request ( 'user' , '' ) ) ;
	$tile = get_request ( 'tile' , 0 ) * 1 ;
	$decision = get_request ( 'decision' , '' ) ;
	
	$uid = getUID ( $db , $user ) ;
	
	$sql = "UPDATE coord_no_admin_unit_museums SET done=1 WHERE id=$tile" ;
	$db->query($sql) ;
	$out['sql'][] = $sql ;
	
    }


} else {
	$out['error'] = "No valid action!" ;
}


print $callback . '(' ;
print json_encode ( $out ) ;
print ")\n" ;

?>

<?PHP

require_once ( './game1.inc' ) ;
require_once ( './php/wikidata.php' ) ;

header('Content-type: application/json');

$callback = get_request ( 'callback' , '' ) ;
$action = get_request ( 'action' , '' ) ;

$out = array () ;
$wil = new WikidataItemList ;
$strings = array (
	"male" => array ( "es" => "Masculino", "pt" => "Masculino", "en" => "Male" ),
	"female" => array ( "es" => "Femenino", "pt" => "Feminino", "en" => "Female" ),
	"intersex" => array ( "es" => "Intersexual", "pt" => "Intersexual", "en" => "Intersex" ),
	"transmale" => array ( "es" => "Hombre transgénero", "pt" => "Homem transgénero", "en" => "Trans Male" ),
	"transfemale" => array ( "es" => "Mujer transgénero", "pt" => "Mulher transgénero", "en" => "Trans Female" ),
	"nonbinary" => array ( "es" => "No binario", "pt" => "Não-binário", "en" => "Non-binary" ),
	"notapplicable" => array ( "es" => "No aplica", "pt" => "Não aplicável", "en" => "Non Applicable" ),
	"notlisted" => array ( "es" => "No está listado, o no aplica", "pt" => "Não está listado, ou não aplica", "en" => "Not listed, or does not apply" ),
	"skip" => array ( "es" => "Saltear", "pt" => "Pular", "en" => "Skip" ),
	"genderless" => array ( "es" => "Personas sin género", "pt" => "Pessoas sem gênero", "en" => "Genderless people" ),
	"adminunit" => array ( "es" => "Lugares sin entidad administrativa", "pt" => "Lugares sem entidade administrativa", "en" => "Places without administrative unit" )
	);

if ( $action == 'desc' ) {
	$lang = get_request('lang','es') ; // The language to use, with 'es' as fallback

	$out = array (
		"label" => array ( "en" => "Contest: Latin America in Wikidata",
				   "es" => "Concurso: Latinoamérica en Wikidata",
				   "pt" => "Concurso: América Latina na Wikidata" ) ,
		"description" => array ( "en" => "Set gender for people or administrative units for Latin American places.<br>
The contest will be from September 30 at 00:01 to October 20 until 23:59 (UTC). [https://www.wikidata.org/wiki/Wikidata:Concurso_%22Latinoam%C3%A9rica_en_Wikidata%22 More info].",
					 "es" => "El concurso tiene como objetivo mejorar información en Wikidata sobre personas y lugares de Latinoamérica. 
Hay dos categorías de juegos:
<ul>
<li>Personas sin género: hay que especificar el sexo o género de la persona.</li>
<li>Lugares sin entidad administrativa: seleccionar la entidad territorial administrativa a la que pertenece. Debe seleccionarse la dependencia inmediatamente superior. Ej: en Argentina es preferible seleccionar, si existe, un municipio/departamento/partido antes que la provincia; en Bolivia es preferible seleccionar la provincia ya que son más pequeñas que los departamentos.</li>
</ul>
El concurso será desde el 30 de septiembre a las 00:01 al 20 de octubre hasta las 23:59 (UTC).
Solo se tomará en cuenta las ediciones realizadas en ese período.
[https://www.wikidata.org/wiki/Wikidata:Concurso_%22Latinoam%C3%A9rica_en_Wikidata%22 Más info]",
				         "pt" => "O concurso tem como objetivo melhorar as informações no Wikidata sobre pessoas e lugares na América Latina.
Há duas categorias de jogos:
<ul>
<li>Pessoas sem gênero: você tem que especificar o sexo ou gênero da pessoa.
<li> Lugares sem entidade administrativa: selecione a entidade territorial administrativa à qual a pessoa pertence. A próxima unidade superior deve ser selecionada.
</ul>
O concurso será realizado de 30 de setembro às 00:01 a 20 de outubro até as 23:59 (UTC).
Somente as edições feitas durante este período serão levadas em consideração.
[https://www.wikidata.org/wiki/Wikidata:Concurso_%22Latinoam%C3%A9rica_en_Wikidata%22 Mais informações]" ) ,
		"icon" => 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/Latin_America_%28orthographic_projection%29.svg/120px-Latin_America_%28orthographic_projection%29.svg.png',
//                "options" => array (
//		        array ( 'name' => 'Juego' , 'key' => 'type' , 'values' => array ( 'genderless' => $strings["genderless"][$lang], 'adminunit' => $strings["adminunit"][$lang] ) )
//		)

	) ;

} else if ( $action == 'tiles' ) {
	$db = openToolDB ( 'wdgames' ) ;
	$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;

	// GET parameters
	$num = get_request('num',1)*1 ; // Number of games to return
	$lang = get_request('lang','es') ; // The language to use, with 'es' as fallback; ignored in this game
//	$type = get_request('type','any') ; // The type of game
	$type = 'adminunit';
	$hadthat = array() ;

	$out['tiles'] = array() ;
	while ( count($out['tiles']) < $num ) {
	   if( $type == 'genderless' ){
		$r = rand() / getrandmax() ;
		$tmp = array() ;
//		$use_labels = array() ;
		$sqls = array() ;
		$sql = "select * from gender_latam WHERE done=0 and random >= $r " ;
		if ( count ( $hadthat ) > 0 ) $sql .= " AND id NOT IN (" . implode(',',$hadthat) . ") " ;
		$sql .= " order by random limit " . ($num*2) ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($o = $result->fetch_object()){
			// Sanity checks
			if ( isDeleted ( $dbwd , $o->item ) ) {
				$sqls[] = "UPDATE gender_latam SET done=1 WHERE id=" . $o->id ;
				continue ;
			}
			if ( isRedirect ( $dbwd , $o->item ) ) {
				$sqls[] = "UPDATE gender_latam SET done=1 WHERE id=" . $o->id ;
				continue ;
			}
			if ( !hasLink ( $dbwd , $o->item , 'Q5' ) ) { // No human
				$sqls[] = "UPDATE gender_latam SET done=1 WHERE id=" . $o->id ;
				continue ;
			}
			if ( hasLink ( $dbwd , $o->item , 'P21' ) ) { // Has gender
				$sqls[] = "UPDATE gender_latam SET done=1 WHERE id=" . $o->id ;
				continue ;
			}
			
			$tmp[] = $o ;
		}

//		$wil->loadItems ( $use_labels ) ;
		
		foreach ( $sqls AS $sql ) $db->query($sql) ; // Clean-up

		foreach ( $tmp AS $o ) {
			$hadthat = $o->id ;
			$g = array(
				'id' => $o->id ,
				'sections' => array () ,
				'controls' => array ()
			) ;
		
			$q = 'Q'.$o->item ;

			$male = array ( 'type' => 'green' , 'decision' => 'done' , 'label' => $strings["male"][$lang] , 'api_action' => 
				array ('action'=>'wbcreateclaim','entity'=>"$q",'snaktype'=>'value','property'=>'P21',
					'value'=>'{"entity-type":"item","numeric-id":6581097}'
				)
			) ;
			$female = array ( 'type' => 'green' , 'decision' => 'done' , 'label' => $strings["female"][$lang] , 'api_action' => 
			array ('action'=>'wbcreateclaim','entity'=>"$q",'snaktype'=>'value','property'=>'P21',
				'value'=>'{"entity-type":"item","numeric-id":6581072}'
			)
			) ;
			$intersex = array ( 'type' => 'green' , 'decision' => 'done' , 'label' => $strings["intersex"][$lang] , 'api_action' => 
			array ('action'=>'wbcreateclaim','entity'=>"$q",'snaktype'=>'value','property'=>'P21',
				'value'=>'{"entity-type":"item","numeric-id":1097630}'
			)
			) ;
			$transfemale = array ( 'type' => 'green' , 'decision' => 'done' , 'label' => $strings["transfemale"][$lang] , 'api_action' => 
			array ('action'=>'wbcreateclaim','entity'=>"$q",'snaktype'=>'value','property'=>'P21',
				'value'=>'{"entity-type":"item","numeric-id":1052281}'
			)
			) ;
			$transmale = array ( 'type' => 'green' , 'decision' => 'done' , 'label' => $strings["transmale"][$lang] , 'api_action' => 
			array ('action'=>'wbcreateclaim','entity'=>"$q",'snaktype'=>'value','property'=>'P21',
				'value'=>'{"entity-type":"item","numeric-id":2449503}'
			)
			) ;
			$nonbinary = array ( 'type' => 'green' , 'decision' => 'done' , 'label' => $strings["nonbinary"][$lang] , 'api_action' => 
			array ('action'=>'wbcreateclaim','entity'=>"$q",'snaktype'=>'value','property'=>'P21',
				'value'=>'{"entity-type":"item","numeric-id":48270}'
			)
			) ;
		
			$g['sections'][] = array ( 'type' => 'item' , 'q' => $q ) ;
			$g['controls'][] = array (
				'type' => 'buttons' ,
				'entries' => array (
					$male ,
					$female ,
					$intersex ,
					$transfemale ,
					$transmale ,
					$nonbinary ,
					array ( 'type' => 'white' , 'decision' => 'skip' , 'label' => $strings['skip'][$lang] ), 
					array ( 'type' => 'blue' , 'decision' => 'no' , 'label' => $strings['notapplicable'][$lang] )
				)
			) ;
		
			$out['tiles'][] = $g ;
			
			if ( count($out['tiles']) == $num ) break ;
		}
	    }
	    else {
		$r = rand() / getrandmax() ;
		$tmp = array() ;
		$sqls = array() ;
		$use_labels = array() ;
		$sql = "SELECT * FROM coord_no_admin_unit WHERE done=0 AND random >= $r " ;
		if ( count ( $hadthat ) > 0 ) $sql .= " AND id NOT IN (" . implode(',',$hadthat) . ") " ;
		$sql .= " ORDER BY random LIMIT " . ($num*2) ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($o = $result->fetch_object()){
			// Sanity checks
			if ( isDeleted ( $dbwd , $o->q ) or isRedirect ( $dbwd , $o->q ) or hasLink ( $dbwd , $o->q , 'P131' ) ) {
				$sqls[] = "UPDATE coord_no_admin_unit SET done=1 WHERE id=" . $o->id ;
				continue ;
			}
			$o->candidates = explode ( ',' , $o->candidates ) ;
			$tmp[] = $o ;
			foreach ( $o->candidates AS $c ) $use_labels["Q$c"] = "Q$c" ;
		}
		foreach ( $sqls AS $sql ) $db->query($sql) ; // Clean-up
		if ( count($tmp) == 0 ) continue ;
		
		$wil->loadItems ( $use_labels ) ;

		foreach ( $tmp AS $o ) {
			$hadthat[] = $o->id ;
			$g = array(
				'id' => $o->id ,
				'sections' => array () ,
				'controls' => array ()
			) ;
		
			$q = 'Q'.$o->q ;
			
			$entries = array() ;
			foreach ( $o->candidates AS $c ) {
				$i = $wil->getItem($c) ;
				$label = "Q$c" ;
				if ( $label == $q ) continue ; // Not in self...
				if ( isset($i) ) $label = $i->getLabel($lang) ;
				$a = array ( 'type' => 'green' , 'decision' => 'yes' , 'label' => $label , 'api_action' => 
					array ('action'=>'wbcreateclaim','entity'=>"$q",'snaktype'=>'value','property'=>'P131',
						'value'=>'{"entity-type":"item","numeric-id":'.$c.'}'
					)
				) ;
				$entries[] = $a ;
			}
		
			$g['sections'][] = array ( 'type' => 'map' , 'lat' => $o->lat , 'lon' => $o->lon , 'zoom' => $o->zoom ) ;
			$g['sections'][] = array ( 'type' => 'item' , 'q' => $q ) ;
			$g['controls'][] = array ( 'type' => 'buttons' , 'entries' => $entries ) ;
			$g['controls'][] = array (
				'type' => 'buttons' ,
				'entries' => array (
					array ( 'type' => 'white' , 'decision' => 'skip' , 'label' => $strings['skip'][$lang] ) ,
					array ( 'type' => 'blue' , 'decision' => 'no' , 'label' => $strings['notlisted'][$lang] )
				)
			) ;
		
			$out['tiles'][] = $g ;
			
			if ( count($out['tiles']) == $num ) break ;
		}
	    }
	}

} else if ( $action == 'log_action' ) {

    $type = get_request('type','any') ; // The type of game

    if( $type == 'genderless' ){
	$ts = date ( 'YmdHis' ) ;
	$db = openToolDB ( 'wdgames' ) ;
	$user = $db->real_escape_string ( get_request ( 'user' , '' ) ) ;
	$tile = get_request ( 'tile' , 0 ) * 1 ;
	$decision = get_request ( 'decision' , '' ) ;
	
	$uid = getUID ( $db , $user ) ;

	$sql = "UPDATE gender_latam SET user=$uid,timestamp='$ts',done=1 WHERE id=$tile AND done=0" ;
	$db->query($sql) ;
	$out['sql'][] = $sql ;
    }
    else {
	$db = openToolDB ( 'wdgames' ) ;
	$user = $db->real_escape_string ( get_request ( 'user' , '' ) ) ;
	$tile = get_request ( 'tile' , 0 ) * 1 ;
	$decision = get_request ( 'decision' , '' ) ;
	
	$uid = getUID ( $db , $user ) ;
	
	$sql = "UPDATE coord_no_admin_unit SET done=1 WHERE id=$tile" ;
	$db->query($sql) ;
	$out['sql'][] = $sql ;
	
    }


} else {
	$out['error'] = "No valid action!" ;
}


print $callback . '(' ;
print json_encode ( $out ) ;
print ")\n" ;

?>

A Wikidata Game API for use in [Distributed Wikidata Game](https://wikidata-game.toolforge.org/distributed)

Based on Magnus Manske's work: https://bitbucket.org/magnusmanske/wikidata-game/src/master/
